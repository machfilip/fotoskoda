import urllib
import re
from bs4 import BeautifulSoup
from inspect import getsourcefile
import sys
import os
from urllib.parse import urlparse
import urllib.request
import mysql.connector
import yaml

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]

conf = yaml.load(open(parent_dir+'/conf/application.yml'))

secret_user = conf['aws']['fotoskoda']['username']
secret_password = conf['aws']['fotoskoda']['password']
secret_database = conf['aws']['fotoskoda']['database']
secret_host = conf['aws']['fotoskoda']['host']
secret_port = conf['aws']['fotoskoda']['port']

url = 'https://bazar.fotoskoda.cz/default.asp?fc=1&ids=234&kdehle=234&r=50'
o = urlparse(url)

db_url = o.scheme+"://"+o.netloc+"/"

category = 'Fotoskoda objektivy'

with urllib.request.urlopen(url) as response:
   html = response.read()

soup = BeautifulSoup(html, "html.parser")
l = soup.find(id='obsah2')
content = l.find_all("div",class_="bazarU");

db_url_single = "";
db_title = "";
db_content = "";
db_price = "";

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)

cursor = cnx.cursor()

for inzerat in content:
	for title in inzerat.find_all("h2",class_="bazarTitleContent"):
		db_url_single = db_url+title.a['href']
		db_title = title.a.string
		db_title = str(db_title)

	for bazarBody in inzerat.find_all("div",class_="bazarText"):
		db_content = bazarBody.get_text()
	
	for bazarPrice in inzerat.find_all("div",class_="bazarPrice"):
		db_price = bazarPrice.get_text()
		db_price = db_price.replace(" ", "")
		db_price = db_price.replace(",-sDPH","")
		db_price = re.findall(r"\d+",db_price)
		db_price = ''.join(db_price)

	query1 = "SELECT * FROM fotoskoda.Record WHERE URL = '"+db_url_single+"';"
	cursor.execute(query1)
	row = cursor.fetchall()

	if len(row) == 0:
		query = "INSERT INTO Record (URL, TITLE, CONTENT, PRICE, CATEGORY) VALUES (%s, %s, %s, %s, %s)"
		cursor.execute(query, (db_url_single, db_title, db_content, db_price, category))
		cnx.commit()

cursor.close()
cnx.close()
