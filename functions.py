#!/usr/bin/python
#from smtplib import SMTP # Standard connection
def sendMail(message):
	from smtplib import SMTP_SSL as SMTP #SSL connection
	#from email.mime.MIMEMultipart import MIMEMultipart
	from email.mime.multipart import MIMEMultipart
	from email.mime.text import MIMEText
	import yaml
	from inspect import getsourcefile
	import os.path
	import sys

	current_path = os.path.abspath(getsourcefile(lambda:0))
	current_dir = os.path.dirname(current_path)
	parent_dir = current_dir[:current_dir.rfind(os.path.sep)]	

	conf = yaml.load(open(parent_dir+'/conf/application.yml'))

	sender = conf['gmail']['username']
	receivers = 'machfilip@gmail.com'

	msg = MIMEMultipart()
	msg['From'] = sender
	msg['To'] = receivers
	msg['Subject'] = 'Nove inzeraty na Fotoskoda.cz! - AWS'
	msg.attach(MIMEText(message,'html'))

	ServerConnect = False
	try:
		secret_sender = conf['gmail']['username']
		secret_password = conf['gmail']['password']

		smtp_server = SMTP('smtp.gmail.com','465')
		smtp_server.login(secret_sender, secret_password)
		ServerConnect = True
	except SMTPHeloError as e:
		print("Server did not reply")
		return 0
	except SMTPAuthenticationError as e:
		print("Incorrect username/password combination")
		return 0
	except SMTPException as e:
		print("Authentication failed")
		return 0

	if ServerConnect == True:
		try:
			smtp_server.sendmail(sender, receivers, msg.as_string())
			print("Successfully sent email")
		except SMTPException as e:
			print("Error: unable to send email", e)
		finally:
			smtp_server.close()
			return 1
