
from functions import sendMail
import mysql.connector
import sys
import yaml
from inspect import getsourcefile
import os.path
import sys

current_path = os.path.abspath(getsourcefile(lambda:0))
current_dir = os.path.dirname(current_path)
parent_dir = current_dir[:current_dir.rfind(os.path.sep)]

conf = yaml.load(open(parent_dir+'/conf/application.yml'))

secret_user = conf['aws']['fotoskoda']['username']
secret_password = conf['aws']['fotoskoda']['password']
secret_database = conf['aws']['fotoskoda']['database']
secret_host = conf['aws']['fotoskoda']['host']
secret_port = conf['aws']['fotoskoda']['port']

cnx = mysql.connector.connect(user=secret_user, password=secret_password, database=secret_database, host=secret_host, port=secret_port)

cursor = cnx.cursor()

query = ("SELECT * FROM fotoskoda.Record WHERE Send_Flag='N' ORDER BY CATEGORY, TITLE;")
cursor.execute(query)

rows = cursor.fetchall()

if len(rows) == 0:
	sys.exit()

message = "<html><body><table>"
for row in rows:
	link = "<tr><td><a href='"+row[1].decode("utf-8")+"'>"+row[2].decode("utf-8")+"</a></td><td>"+row[3].decode("utf-8")+"</td><td>"+str(row[4])+"</td><td>"+row[5].decode("utf-8")+"</td></tr>"
	message = message + link

message = message+"</table></body></html>"

i = sendMail(message)
if i == 1:
	query = ("UPDATE fotoskoda.Record SET Send_Flag = 'Y' WHERE Send_Flag = 'N';")
	cursor.execute(query)
	cnx.commit()

cursor.close()
cnx.close()
